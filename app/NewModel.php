<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewModel extends Model
{
    protected $table = 'name';
    protected $fillable = ['title', 'description', 'price', 'availability'];
}
