<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Product;

class ProductsController extends Controller
{
 
    public function index()
    {
    	$data = Product::all();
		dd($data);

        return $data;
    }
 
    public function show(Product $product)
    {
        return $product;
    }
 
    public function store(Request $request)
    {
        $product = Product::create($request->all());
 
        return response()->json($product, 201);
    }
 
    public function update(Request $request, Product $product)
    {
        $product->update($request->all());
 
        return response()->json($product, 200);
    }
 
    public function delete(Product $product)
    {
        $product->delete();
 
        return response()->json(null, 204);
    }

}
