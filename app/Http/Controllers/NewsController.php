<?php

namespace App\Http\Controllers;

use DB;
use App\NewModel;
use Illuminate\Http\Request;

class NewsController extends Controller
{

    public function index()
    {
        $tasks = DB::table('name')->get();
        return view('tasks.index', compact('tasks'));
    }

    public function show($id){
        $task = DB::table('name')->find($id);
        return view('tasks.show', compact('task'));
    }

    public function editView($id)
    {
        $task = NewModel::find($id);
        return view('tasks.edit_view', compact('task'));
    }

    public function save(Request $request, $id)
    {
        $task = new NewModel;

        if($id != 'new') {
            $task = NewModel::find($id);
        }

        $task->name = $request->name;
        $task->text = $request->text;
        $task->save();
        return redirect()->route('news');
    }

    public function createView(){
        return view('tasks.create');
    }

    public function delete($id){
        $task = NewModel::find($id);
        $task->delete();
        return redirect()->route('news');
    }

   


}
