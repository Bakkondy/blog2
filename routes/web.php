<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('products', 'ProductsController@index');
 
Route::get('products/{product}', 'ProductsController@show');
 
Route::post('products','ProductsController@store');
 
Route::put('products/{product}','ProductsController@update');
 
Route::delete('products/{product}', 'ProductsController@delete');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



//Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middleware'=>['auth']], function(){
//	Route::get('', 'DashboardController@dashboard')->name('admin.index');
//	Route::resource('/category', 'CategoryController', ['as'=>'admin']);
//	Route::resource('/article', 'ArticleController', ['as'=>'admin']);
//});

Route::group(['prefix'=>'news'], function (){
    Route::get('/', 'NewsController@index')->name('news');
    Route::get('create', 'NewsController@createView')->name('news_create_view');
    Route::get('{task}', 'NewsController@show')->name('news_show');
    Route::get('{task}/edit', 'NewsController@editView')->name('news_edit_view');
    Route::post('{task}/save', 'NewsController@save')->name('news_save');
    Route::get('{task}/delete', 'NewsController@delete')->name('news_delete');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
