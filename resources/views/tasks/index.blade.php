
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<ul>
    @foreach ($tasks as $task)
        <li><a href="{{route('news_show', $task->id)}}">({{$task->name}})</a> <a href="{{route('news_edit_view', $task->id)}}">edit</a> <a href="{{route('news_delete', $task->id)}}">delete</a></li>
    @endforeach

        <a href="{{route('news_create_view')}}">Create new</a>
</ul>
</body>
</html>